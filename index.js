const express = require('express');
const app = express();
const { PORT = 8080 } = process.env;
const path = require('path');

// Logic goes here


// Allow the express server to share static content (CSS, JS)
app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

//Default route for your website
app.get('/', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'index.html'));
})

// Route for the restaurants page
app.get('/restaurants', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'restaurants.html'));
})

// Route for the raw JSON file
app.get('/data', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'data.json'));
})

app.listen(PORT, () => console.log('Server started on port ${PORT}...'));